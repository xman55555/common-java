package net.gdface.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;

public class BeanPropertyUtils {

	private static final boolean hasReadMethod(PropertyDescriptor propertyDescriptor) {
		Method m = propertyDescriptor.getReadMethod();
		// 过滤掉Object中的get方法
		if (m != null && m.getDeclaringClass() != Object.class) {
			return true;
		}
		return false;
	}

	private static final boolean hasWriteMethod(PropertyDescriptor propertyDescriptor) {
		Method m = propertyDescriptor.getWriteMethod();
		// 过滤掉Object中的get方法
		if (m != null && m.getDeclaringClass() != Object.class) {
			return true;
		}
		return false;
	}

	/**
	 * 获取beanClass中所有具有指定读写类型(rw)的属性
	 * @param beanClass
	 * @param rw 属性类型标记 <br>
	 * 					<li>0 所有属性</li>
	 * 					<li>1 读属性</li>
	 * 					<li>2 写属性</li>
	 * 					<li>3 读写属性</li>
	 * @param lenient 是否为宽容模式---允许返回类型不为void的setter方法
	 * @return 属性名与PropertyDescriptor映射的Map对象
	 */
	public static final Map<String, PropertyDescriptor> getProperties(Class<?> beanClass, int rw,boolean lenient) {
		try {
			Map<String, PropertyDescriptor> properties = new HashMap<String, PropertyDescriptor>();
			if (beanClass != null) {
				BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
				PropertyUtilsBean propertyUtils = beanUtils.getPropertyUtils();
				PropertyDescriptor[] origDescriptors = propertyUtils.getPropertyDescriptors(beanClass);
				Boolean put;
				for (PropertyDescriptor pd : origDescriptors) {
					if(lenient){
						pd = LenientDecoratorOfDescriptor.toDecorator(pd);
					}
					put = false;
					switch (rw &= 3) {
					case 0:
						put = hasReadMethod(pd) || hasWriteMethod(pd);
						break;
					case 1:
						put = hasWriteMethod(pd);
						break;
					case 2:
						put = hasReadMethod(pd);
						break;
					case 3:
						put = hasReadMethod(pd) && hasWriteMethod(pd);
						break;
					}
					if (put) {
						properties.put(pd.getName(), pd);
					}
				}
			}
			return properties;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	/**
	 * 获取beanClass中所有具有指定读写类型(rw)的属性
	 * @param beanClass
	 * @param rw 属性类型标记 <br>
	 * 					<li>0 所有属性</li>
	 * 					<li>1 读属性</li>
	 * 					<li>2 写属性</li>
	 * 					<li>3 读写属性</li>
	 * @return 属性名与PropertyDescriptor映射的Map对象
	 */
	public static final Map<String, PropertyDescriptor> getProperties(Class<?> beanClass, int rw) {
		return getProperties(beanClass, rw, false);
	}
	public static final <T>T copy(T from,T to){
		if(null==from||null==to)
			throw new NullPointerException();
		PropertyUtilsBean propertyUtils = BeanUtilsBean.getInstance().getPropertyUtils();
		try {
			propertyUtils.copyProperties(to, from);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
		return to;
	}
	/**
	 * 从{@code from}复制共有的(可读写字段)字段到{@code to},{@code from}和{@code to}可以是两个互不继承的类型<br>
	 * 要求两个类型共有的字段类型必须一致，否则抛出异常
	 * @param from Java Bean
	 * @param to    Java Bean
	 * @param ignoreNull 为{@code true}忽略为{@code null}的字段
	 * @param  ignoreEmpty 为{@code true}忽略为空的String类型字段或
	 * 										{@link Collection},{@link Iterable},{@link Iterator},{@link Map},数组类型字段,
	 * 										{@code ignoreNull}为{@code true}时有效
	 */
	public static final void copyBean(Object from,Object to, boolean ignoreNull, boolean ignoreEmpty){
		if(null==from || null == to)
			throw new NullPointerException("from or to is null");
		Map<String, PropertyDescriptor> fromProps = getProperties(from.getClass(),1);
		Map<String, PropertyDescriptor> toProps = getProperties(to.getClass(),2);
		// 共同有的字段集合
		HashSet<String> intersection = new HashSet<String>(fromProps.keySet());
		intersection.retainAll(toProps.keySet());
		PropertyUtilsBean propertyUtils = BeanUtilsBean.getInstance().getPropertyUtils();
		try {
			// 遍历所有共有字段
			for(String fieldName : intersection){
				// 从from中读取字段值
				Object value = propertyUtils.getProperty(from, fieldName);
				if(!ignoreNull || null != value){
					if(!ignoreEmpty || !isEmpty(value)){
						// 向 to 写入字段字段值
						propertyUtils.setProperty(to,fieldName,value);
					}
				}
			}
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 判断输入参数是否为{@code null}或空<br>
	 * 如果输入参数为@{@link String},{@link Collection},{@link Iterable},{@link Iterator},{@link Map},数组类型则返回其是否为空,
	 * 否则返回{@code false}
	 * @param value 为{@code null}返回{@code true}
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isEmpty(Object value){
		if(null == value){
			return true;
		}else	if(value instanceof String){
			return ((String)value).isEmpty();
		}else if (value instanceof Collection) {
			return ((Collection)value).isEmpty();
		}else if (value instanceof Iterator) {
			return !((Iterator)value).hasNext();
		}else if (value instanceof Iterable) {
			return !((Iterable)value).iterator().hasNext();
		}else if (value instanceof Map) {
			return ((Map)value).isEmpty();
		}else if (value.getClass().isArray()) {
			return Array.getLength(value)==0;
		}
		return false;
	}
}
