package net.gdface.utils;

import static com.google.common.base.Strings.nullToEmpty;

import com.google.common.base.CaseFormat;

public class CaseSupport {

	/**
	 * @param name
	 * @return 将变量名转为蛇形命名法格式的字符串
	 */
	public static String toSnakecase(String name){
		return null == name ? name : CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE,name);
	}

	/**
	 * @param name
	 * @return 将变量名转为驼峰命名法格式的字符串
	 */
	public static String toCamelcase(String name){
		return null == name ? name : CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, name);
	}

	/**
	 * 判断 变量是否为驼峰命名法格式的字符串
	 * @param name
	 */
	public static boolean isCamelcase(String input){
		if(!nullToEmpty(input).trim().isEmpty()){
			return !input.equals(input.toLowerCase()) 
					&& !input.equals(input.toUpperCase()) && input.indexOf('_')<0; 
		}
		return false;
	}
	/**
	 * 判断 变量是否为驼峰命名法格式的字符串
	 * @param input
	 */
	public static boolean isSnakelcase(String input){
		if(!nullToEmpty(input).trim().isEmpty()){
			return input.indexOf('_')>=0;
		}
		return false ;
	}

}
