package net.gdface.utils;

import com.google.common.base.Function;

public interface ClassCommentProviderFactory extends Function<Class<?>, ClassCommentProvider> {

}
