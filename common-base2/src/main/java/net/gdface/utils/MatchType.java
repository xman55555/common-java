package net.gdface.utils;

/**
 * 规则匹配模式定义<br>
 * 定义字符串的匹配方式
 * @author guyadong
 *
 */
public enum MatchType{
	/** 默认:字符串比较 */ DEFAULT,
	/** 字符串左侧比较(String.startsWith) */ LEFT,
	/** 字符串右侧比较(String.endsWith) */ RIGHT,
	/** 字符串包含匹配 */ INCLUDE,
	/** 正则表达式匹配 */ REGEX;
	/**
	 * 检查输入参数({@code input})是否匹配模式{@code pattern}
	 * @param input
	 * @param pattern
	 */
	public boolean match(String input,String pattern) {
		if(null != input && null != pattern) {
			switch (this) {
			case LEFT:
				return input.startsWith(pattern);
			case RIGHT:
				return input.endsWith(pattern);
			case INCLUDE:
				return input.indexOf(pattern) >= 0;
			case REGEX:
				return input.matches(pattern);
			default:
				return input.equals(pattern);
			}
		}
		return false;
	}
}