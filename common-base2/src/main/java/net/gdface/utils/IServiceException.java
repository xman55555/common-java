package net.gdface.utils;

public interface IServiceException {
	/**
	 * 返回服务端异常堆栈信息
	 * @return 服务端异常堆栈信息
	 */
	String getServiceStackTraceMessage();
}
