package com.facebook.swift.codec.metadata;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.google.common.base.MoreObjects;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

public class NameIncludeFilter implements Predicate<String> {
	private final Set<String> includeMethods;

	public NameIncludeFilter(Set<String> includeNames) {
		super();
		this.includeMethods = MoreObjects.firstNonNull(includeNames,Collections.<String>emptySet());
	}
	public NameIncludeFilter(Iterable<String> excludeNames) {
		this(Sets.newLinkedHashSet(excludeNames));
	}
	public NameIncludeFilter(String ...excludeNames) {
		this(Arrays.asList(excludeNames));
	}
	@Override
	public boolean apply(String input) {
		if(includeMethods.contains(input)){
			return true;
		}
		for(String pattern:includeMethods){
			if(PatternFilter.filter(pattern,input,false)){
				return true;
			}
		}
		return false;
	}
	/**
	 * @return includeMethods
	 */
	public Set<String> getIncludeMethods() {
		return includeMethods;
	}

	
}
