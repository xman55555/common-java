package com.facebook.swift.codec.metadata;

import com.google.common.base.Function;

import net.gdface.utils.ClassCommentProvider;

public class Decorators {

	/**
	 * {@link ClassCommentProvider}工厂接口实例
	 * 当前字段不为{@code null}，且没有定义找到{@link com.facebook.swift.codec.ThriftDocumentation}定义的文档数据时,
	 * 使用javadoc从源码读取的注释作为文档数据<br>
	 * 当前引用此变量的项目:codegen,idl-generator
	 */
	public static Function<Class<?>, ClassCommentProvider> javadocCommentProviderFactory = null;

}
