package com.facebook.swift.codec.metadata;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class FieldNameFilters{
	private final Map<Class<?>, List<String>> excludeFields;
	private final LoadingCache<Class<?>,Predicate<String>> 
	FILTERS_CACHE = 
		CacheBuilder.newBuilder().build(
			new CacheLoader<Class<?>,Predicate<String>>(){
				@Override
				public Predicate<String> load(Class<?> structClass) throws Exception {
					for(Entry<Class<?>, List<String>> entry:excludeFields.entrySet()){
						if(entry.getKey().isAssignableFrom(structClass)){
							return new Filter(entry.getValue());
						}
					}
					return Predicates.alwaysTrue();
		}});
	public FieldNameFilters(Map<Class<?>, List<String>> excludeFields) {
		super();
		this.excludeFields = excludeFields;		
	}
	Predicate<String> filterOf(Class<?> structClass){
		return FILTERS_CACHE.getUnchecked(structClass);
	}

	private class Filter implements Predicate<String>{
		final List<String> excludeFields;
		
		public Filter(List<String>excludeFields) {
			super();
			this.excludeFields = excludeFields;
		}

		@Override
		public boolean apply(String input) {
			return !excludeFields.contains(input);
		}
		
	}

	/**
	 * @return excludeFields
	 */
	public Map<Class<?>, List<String>> getExcludeFields() {
		return excludeFields;
	}
}
