package com.facebook.swift.codec.metadata;

import com.facebook.swift.codec.ThriftProtocolType;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;

import static com.google.common.base.Preconditions.checkArgument;

public enum ErpcProtocolType {
    UNKNOWN(false),
    BOOL(true),
    INT8(true),
    INT16(true),
    INT32(true),
    INT64(true),
    UINT8(true),
    UINT16(true),
    UINT32(true),
    UINT64(true),
    FLOAT(true),
    DOUBLE(true),
    STRING(true),
    STRUCT(false),
    LIST(false),
    ENUM(false), 
    BINARY(true),
    UNION(false);
	public final boolean simpleType;
	private ErpcProtocolType(boolean simpleType){
		this.simpleType = simpleType;
	}
	public boolean isString(){
		return this.equals(STRING);
	}
	public boolean isStruct(){
		return this.equals(STRUCT);
	}
	public boolean isList(){
		return this.equals(LIST);
	}
	public boolean isEnum(){
		return this.equals(ENUM);
	}
	public boolean isBinary(){
		return this.equals(BINARY);
	}
	public boolean isUnion(){
		return this.equals(UNION);
	}	
	
	/**
	 * @return simpleType
	 */
	public boolean isSimpleType() {
		return simpleType;
	}

	private static final BiMap<ThriftProtocolType, ErpcProtocolType> THRIFT_MATCHED_TYPES = ImmutableBiMap.<ThriftProtocolType, ErpcProtocolType>builder() 
			.put(ThriftProtocolType.BOOL, ErpcProtocolType.BOOL)
			.put(ThriftProtocolType.BYTE, ErpcProtocolType.UINT8)
			.put(ThriftProtocolType.DOUBLE, ErpcProtocolType.DOUBLE)
			.put(ThriftProtocolType.I16, ErpcProtocolType.INT16)
			.put(ThriftProtocolType.I32, ErpcProtocolType.INT32)
			.put(ThriftProtocolType.I64, ErpcProtocolType.INT64)
			.put(ThriftProtocolType.STRING, ErpcProtocolType.STRING)
			.put(ThriftProtocolType.BINARY, ErpcProtocolType.BINARY)
			.put(ThriftProtocolType.STRUCT, ErpcProtocolType.STRUCT)
			.put(ThriftProtocolType.ENUM, ErpcProtocolType.ENUM)
			.put(ThriftProtocolType.LIST, ErpcProtocolType.LIST)
			.build();

	public static ErpcProtocolType erpcTypeOf(ThriftProtocolType protocolType){
		if(null != protocolType){
			ErpcProtocolType erpcProtocolType = THRIFT_MATCHED_TYPES.get(protocolType);
			checkArgument(null != erpcProtocolType,"UNSUPPORT THRIFT TYPE %s to cast ERPC type",protocolType);
			return erpcProtocolType;
		}
		return null;
	}
}
