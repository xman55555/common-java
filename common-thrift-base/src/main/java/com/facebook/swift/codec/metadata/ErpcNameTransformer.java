package com.facebook.swift.codec.metadata;

import com.google.common.base.Function;

public class ErpcNameTransformer implements  Function<String, String>{
	public static final Function<String, String> erpcParemeterNameTransformer = new ErpcNameTransformer( "_%s");
	public static final Function<String, String> erpcFieldNameTransformer = new ErpcNameTransformer( "m_%s");
	public static final Function<String, String> erpcTypeNameTransformer = new ErpcNameTransformer( "%s_t");    	
	private final String fmt;
	public ErpcNameTransformer(String fmt) {
		this.fmt = fmt;
	}

	@Override
	public String apply(String input) {
		return String.format(fmt, input);
	}   
}