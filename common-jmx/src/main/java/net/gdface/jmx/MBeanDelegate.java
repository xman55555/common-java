package net.gdface.jmx;

/**
 * 实现MBean接口封装<br>
 * 
 * @author guyadong
 *
 * @param <I> 接口类型
 */
public class MBeanDelegate<I> extends BaseMBeanDelegate<I>{
	private final I delegate;
	@SuppressWarnings("unchecked")
	public MBeanDelegate(I delegate) {
		super((Class<I>)JMXSupport.getMBeanClass(delegate));
		this.delegate = delegate;
	}
	@Override
	protected Object getMBean(){
		return delegate;
	}
	/**
	 * 返回当前代理实例
	 * @return 代理实例
	 */
	public I delegate() {
		return delegate;
	}
}


