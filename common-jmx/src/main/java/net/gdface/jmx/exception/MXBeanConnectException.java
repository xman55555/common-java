package net.gdface.jmx.exception;

/**
 * MXBean调用连接异常
 * @author guyadong
 *
 */
public class MXBeanConnectException extends MXBeanRuntimeException {

	private static final long serialVersionUID = -1277822236842400930L;

	public MXBeanConnectException(Throwable cause) {
		super(cause);
	}

}
