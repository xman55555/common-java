/**   
* @Title: WorkerManagerFactory.java 
* @Package net.gdface.worker 
* @Description: TODO 
* @author guyadong   
* @date 2015年5月20日 下午7:29:59 
* @version V1.0   
*/
package net.gdface.worker;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author guyadong
 *
 */
public class WorkerManagerFactory {

	/**
	 * 生成固定大小的线程池
	 * 
	 * @param nThreads
	 *            线程数目
	 * @param workerClass
	 *            工作线程类
	 * @param queueManager
	 *            队列管理器对象
	 * @param checkIntervalMills
	 *            监视线程的检查间隔时间(毫秒)
	 * @param name 线程池名字,为null或空使用默认名字
	 * @return {@link WorkerManager}实例
	 */
	public static <T extends WorkData> WorkerManager newFixedThreadPool(int nThreads, Class<? extends Runnable> workerClass,
			QueueManager<T> queueManager, long checkIntervalMills, String name) {
		return new WorkerManagerImpl<T>(nThreads, nThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(),
				workerClass, queueManager, checkIntervalMills, name);
	}

}
