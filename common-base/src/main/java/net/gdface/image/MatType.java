package net.gdface.image;

/**
 * 图像矩阵格式定义
 * @author guyadong
 *
 */
public enum MatType {
	/** NV21 (YUV420sp) YYYYYYYY VUVU */NV21,
	/** RGB 888 */RGB,
	/** BGR 888 */BGR,
	/** RGBA */RGBA,
	/** gray */GRAY
}
