package net.gdface.utils;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayUtils {

	private ArrayUtils() {}
	/**
	 * 清除array中为{@code null}的元素
	 * @param array 输入的数组
	 * @return 如果 array为{@code null}或长度为0或array中没有{@code null}元素则返回array,否则返回新的不含{@code null}的数组
	 */
	@SuppressWarnings("unchecked")
	public static final <T> T[] cleanNull(T[] array){
		if(null == array || array.length == 0){
			return array;
		}		
		T[] copy = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);
		int c = 0;
		for(int i=0; i < array.length; ++i){
			if(null != array[i]){
				copy[c++] = array[i];
			}
		}
		return c == array.length ? array : Arrays.copyOf(copy, c);
	}
    /**
     * 返回第一个为{@code null}元素的索引,没找到返回-1,如果输入为{@code null}则返回-2
     * @param objects
     */
    public static int indexOfFirstNull(Object...objects) {
		if(objects != null){
			for(int i=0;i< objects.length;++i){
				if(null == objects[i]){
					return i;
				}
			}
			return -1;
		}
		return -2;
	}
	/**
	 * @param objects 
	 * @return true if any one of object is null or objects is null 
	 */
	public static boolean hasNull(Object...objects) {
		return indexOfFirstNull(objects) != -1;
	}
	/**
	 * <p>Adds all the elements of the given arrays into a new array.
	 * <p>The new array contains all of the element of {@code array1} followed
	 * by all of the elements {@code array2}. When an array is returned, it is always
	 * a new array.
	 *
	 * <pre>
	 * ArrayUtils.addAll(null, null)     = null
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * ArrayUtils.addAll([null], [null]) = [null, null]
	 * ArrayUtils.addAll(["a", "b", "c"], ["1", "2", "3"]) = ["a", "b", "c", "1", "2", "3"]
	 * </pre>
	 *
	 * @param <T> the component type of the array
	 * @param array1  the first array whose elements are added to the new array, may be {@code null}
	 * @param array2  the second array whose elements are added to the new array, may be {@code null}
	 * @return The new array, {@code null} if both arrays are {@code null}.
	 *      The type of the new array is the type of the first array,
	 *      unless the first array is null, in which case the type is the same as the second array.
	 * @since 2.1
	 * @throws IllegalArgumentException if the array types are incompatible
	 */
	public static <T> T[] addAll(final T[] array1, @SuppressWarnings("unchecked") final T... array2) {
	    if (array1 == null) {
	        return clone(array2);
	    } else if (array2 == null) {
	        return clone(array1);
	    }
	    final Class<?> type1 = array1.getClass().getComponentType();
	    @SuppressWarnings("unchecked") // OK, because array is of type T
	    final T[] joinedArray = (T[]) Array.newInstance(type1, array1.length + array2.length);
	    System.arraycopy(array1, 0, joinedArray, 0, array1.length);
	    try {
	        System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
	    } catch (final ArrayStoreException ase) {
	        // Check if problem was due to incompatible types
	        /*
	         * We do this here, rather than before the copy because:
	         * - it would be a wasted check most of the time
	         * - safer, in case check turns out to be too strict
	         */
	        final Class<?> type2 = array2.getClass().getComponentType();
	        if (!type1.isAssignableFrom(type2)) {
	            throw new IllegalArgumentException("Cannot store " + type2.getName() + " in an array of "
	                    + type1.getName(), ase);
	        }
	        throw ase; // No, so rethrow original
	    }
	    return joinedArray;
	}
	// Clone
	//-----------------------------------------------------------------------
	/**
	 * <p>Shallow clones an array returning a typecast result and handling
	 * {@code null}.
	 *
	 * <p>The objects in the array are not cloned, thus there is no special
	 * handling for multi-dimensional arrays.
	 *
	 * <p>This method returns {@code null} for a {@code null} input array.
	 *
	 * @param <T> the component type of the array
	 * @param array  the array to shallow clone, may be {@code null}
	 * @return the cloned array, {@code null} if {@code null} input
	 */
	public static <T> T[] clone(final T[] array) {
	    if (array == null) {
	        return null;
	    }
	    return array.clone();
	}
}
