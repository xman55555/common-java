package net.gdface.utils;

import java.lang.reflect.Method;

import static net.gdface.utils.ConditionChecks.checkNotNull;
/**
 * 实现接口<I>实例<T>的代理类 <br>
 * @author guyadong
 *
 * @param <I> 接口类型
 * @param <T> 接口实现类型
 */
public class InterfaceDecorator<I,T> extends BaseInterfaceDecorator<I,T>{
	
	/**
	 * 构造方法
	 * 
	 * @param interfaceClass 接口类
	 * @param delegate 实现接口的类
	 * @param allowBreak 为{@code true}时允许 {@link #beforeInvoke(Object, Method, Object[])}抛出异常
	 */
	public InterfaceDecorator(Class<I> interfaceClass, T delegate, boolean allowBreak) {
		super(interfaceClass, delegate, allowBreak);
	}
	/**
	 * 构造方法
	 * 
	 * @param interfaceClass 接口类
	 * @param delegate 实现接口的类
	 */
	public InterfaceDecorator(Class<I> interfaceClass, T delegate) {
		super(interfaceClass, delegate);
	}
	/**
	 * 简化版构造函数<br>
	 * 当delegate只实现了一个接口时，自动推断接口类型
	 * @param delegate
	 */
	public InterfaceDecorator(T delegate) {
		super(delegate);
	}
	
	@Override
	protected Object doInvoke(Object proxy, Method method, Object[] args) throws Throwable {
		return method.invoke(checkNotNull(delegate,"delegate is null"), args);
	}
}
