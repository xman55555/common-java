package net.gdface.utils;

import static net.gdface.utils.ConditionChecks.checkNotNull;
/**
 * 通过{@link VariableGetter}接口实例提供变量值,实现基于volatile的双重检查锁定实现懒加载变量实例<br>
 * 
 * @author guyadong
 *
 * @param <T> variable type
 * @see BaseVolatile
 * @since 2.6.6
 */
public class VolatileVariable<T> extends BaseVolatile<T>{
	private final VariableGetter<T> getter;
	/**
	 * 构造方法
	 * @param getter 提供变量值的实例
	 */
	public VolatileVariable(VariableGetter<T> getter) {
		this.getter = checkNotNull(getter,"getter is null");
	}
	@Override
	protected T doGet() {
		return getter.get();
	}
	public static interface VariableGetter<T>{
		public T get();
	}
}
