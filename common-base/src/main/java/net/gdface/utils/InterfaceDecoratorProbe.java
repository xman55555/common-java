package net.gdface.utils;

import java.lang.reflect.Method;

/**
 * {@link InterfaceDecorator}方法调用侦听器
 * @author guyadong
 *
 */
public interface InterfaceDecoratorProbe{
	/**
	 * 接口方法调用前被调用
	 * @param proxy 接口实例
	 * @param method 接口方法
	 * @param args 方法参数
	 * @throws Exception 抛出异常中断调用
	 */
	void beforeInvoke(Object proxy, Method method, Object[] args) throws Exception;
	/**
	 * 接口方法成功调用后被调用
	 * @param proxy 接口实例
	 * @param method 接口方法
	 * @param args 方法参数
	 * @param result 接口方法调用返回值
	 */
	void onComplete(Object proxy, Method method, Object[] args, Object result);
	/**
	 * 接口方法调用抛出异常时被调用
	 * @param proxy 接口实例
	 * @param method 接口方法
	 * @param args 方法参数
	 * @param error 接口方法调用抛出的异常
	 */
	void onError(Object proxy, Method method, Object[] args, Throwable error);
	/**
	 * 接口方法调用结束时被调用(不论成功或异常)
	 * @param proxy 接口实例
	 * @param method 接口方法
	 * @param args 方法参数
	 */
	void onDone(Object proxy, Method method, Object[] args);
	
	/**
	 * {@link InterfaceDecoratorProbe}的默认实现(DO NOTHING)
	 * @author guyadong
	 *
	 */
	public class DefaultProbe implements InterfaceDecoratorProbe{
	
		@Override
		public void beforeInvoke(Object proxy, Method method, Object[] args) throws Exception {
		}
	
		@Override
		public void onComplete(Object proxy, Method method, Object[] args, Object result) {
			
		}
	
		@Override
		public void onError(Object proxy, Method method, Object[] args, Throwable error) {
		}
	
		@Override
		public void onDone(Object proxy, Method method, Object[] args) {
		}
		
	}
}
