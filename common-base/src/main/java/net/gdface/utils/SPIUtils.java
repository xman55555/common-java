package net.gdface.utils;

import java.util.Iterator;
import java.util.ServiceLoader;

public class SPIUtils {

	private SPIUtils() {
	}
	/**
	 * SPI(Service Provider Interface)机制加载接口实例实例,
	 * 没有找到返回默认实例
	 * @param interfaceClass 接口类
	 * @param defaultInstance 
	 * @return T instance
	 */
	public static <T>T loadProvider(Class<T> interfaceClass,T defaultInstance) {
		Assert.notNull(interfaceClass, "interfaceClass");
		ServiceLoader<T> providers = ServiceLoader.load(interfaceClass);
		Iterator<T> itor = providers.iterator();
		if(!itor.hasNext()){
			return defaultInstance;
		}
		return itor.next();
	}
}
